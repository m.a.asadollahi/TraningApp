package android.maa.traningapp.PublicTools;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Toast;
import android.maa.traningapp.R;

public class PublicMethod {

    public static void toast(Context mcontext, String message) {
        Toast.makeText(mcontext, message, Toast.LENGTH_SHORT).show();
    }

    public static String getWeatherIcon(Context mContext, int weatherCode) {
        String icon = "";

        switch (weatherCode) {

            case 0:
                icon = mContext.getString(R.string.wi_yahoo_0);
                break;
            case 1:
                icon = mContext.getString(R.string.wi_yahoo_1);
                break;
            case 2:
                icon = mContext.getString(R.string.wi_yahoo_2);
                break;
            case 3:
                icon = mContext.getString(R.string.wi_yahoo_3);
                break;
            case 4:
                icon = mContext.getString(R.string.wi_yahoo_4);
                break;
            case 5:
                icon = mContext.getString(R.string.wi_yahoo_5);
                break;
            case 6:
                icon = mContext.getString(R.string.wi_yahoo_6);
                break;
            case 7:
                icon = mContext.getString(R.string.wi_yahoo_7);
                break;
            case 8:
                icon = mContext.getString(R.string.wi_yahoo_8);
                break;
            case 9:
                icon = mContext.getString(R.string.wi_yahoo_9);
                break;
            case 10:
                icon = mContext.getString(R.string.wi_yahoo_10);
                break;
            case 11:
                icon = mContext.getString(R.string.wi_yahoo_11);
                break;
            case 12:
                icon = mContext.getString(R.string.wi_yahoo_12);
                break;
            case 13:
                icon = mContext.getString(R.string.wi_yahoo_13);
                break;
            case 14:
                icon = mContext.getString(R.string.wi_yahoo_14);
                break;
            case 15:
                icon = mContext.getString(R.string.wi_yahoo_15);
                break;
            case 16:
                icon = mContext.getString(R.string.wi_yahoo_16);
                break;
            case 17:
                icon = mContext.getString(R.string.wi_yahoo_17);
                break;
            case 18:
                icon = mContext.getString(R.string.wi_yahoo_18);
                break;
            case 19:
                icon = mContext.getString(R.string.wi_yahoo_19);
                break;
            case 20:
                icon = mContext.getString(R.string.wi_yahoo_20);
                break;
            case 21:
                icon = mContext.getString(R.string.wi_yahoo_21);
                break;
            case 22:
                icon = mContext.getString(R.string.wi_yahoo_22);
                break;
            case 23:
                icon = mContext.getString(R.string.wi_yahoo_23);
                break;
            case 24:
                icon = mContext.getString(R.string.wi_yahoo_24);
                break;
            case 25:
                icon = mContext.getString(R.string.wi_yahoo_25);
                break;
            case 26:
                icon = mContext.getString(R.string.wi_yahoo_26);
                break;
            case 27:
                icon = mContext.getString(R.string.wi_yahoo_27);
                break;
            case 28:
                icon = mContext.getString(R.string.wi_yahoo_28);
                break;
            case 29:
                icon = mContext.getString(R.string.wi_yahoo_29);
                break;
            case 30:
                icon = mContext.getString(R.string.wi_yahoo_30);
                break;
            case 31:
                icon = mContext.getString(R.string.wi_yahoo_31);
                break;
            case 32:
                icon = mContext.getString(R.string.wi_yahoo_32);
                break;
            case 33:
                icon = mContext.getString(R.string.wi_yahoo_33);
                break;
            case 34:
                icon = mContext.getString(R.string.wi_yahoo_34);
                break;
            case 35:
                icon = mContext.getString(R.string.wi_yahoo_35);
                break;
            case 36:
                icon = mContext.getString(R.string.wi_yahoo_36);
                break;
            case 37:
                icon = mContext.getString(R.string.wi_yahoo_37);
                break;
            case 38:
                icon = mContext.getString(R.string.wi_yahoo_38);
                break;
            case 39:
                icon = mContext.getString(R.string.wi_yahoo_39);
                break;
            case 40:
                icon = mContext.getString(R.string.wi_yahoo_40);
                break;
            case 41:
                icon = mContext.getString(R.string.wi_yahoo_41);
                break;
            case 42:
                icon = mContext.getString(R.string.wi_yahoo_42);
                break;
            case 43:
                icon = mContext.getString(R.string.wi_yahoo_43);
                break;
            case 44:
                icon = mContext.getString(R.string.wi_yahoo_44);
                break;
            case 45:
                icon = mContext.getString(R.string.wi_yahoo_45);
                break;
            case 46:
                icon = mContext.getString(R.string.wi_yahoo_46);
                break;
            case 47:
                icon = mContext.getString(R.string.wi_yahoo_47);
                break;
            case 3200:
                icon = mContext.getString(R.string.wi_yahoo_3200);
                break;


        }
        return icon;
    }

    public  static String convertFtoC(String temp){
        String result="0";
        int intTemp= Integer.parseInt(temp);
        int cTemp =(int)((intTemp-32)/1.8);
        result=Integer.toString(cTemp)+"°";
        return  result;
    }

    public  static Typeface weatherTypeFace(Context mContext){
         Typeface typeface = Typeface.createFromAsset(mContext.getAssets(),"fonts/weather.ttf");
        return  typeface;
    }


}

