package android.maa.traningapp.PublicTools;


import android.content.Context;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import org.json.JSONObject;
import cz.msebera.android.httpclient.Header;

public class WeatherClass {

    private String IP_API_URL = "http://ip-api.com/json";
    Context mContext;

    public WeatherClass(Context mContext) {
        this.mContext = mContext;
    }

    void getCityName() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(IP_API_URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethod.toast(mContext, "Error To Connect ....");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                String cityName = parseAndReturnCityName(responseString);
            }
        });
    }

     String parseAndReturnCityName(String webServiceResponse) {
        String result = "";
        try {
            JSONObject jObject = new JSONObject(webServiceResponse);
            result = jObject.getString("city").toLowerCase();
        } catch (Exception error) {
            PublicMethod.toast(mContext, error.getMessage());
        }

        return result;
    }


}
