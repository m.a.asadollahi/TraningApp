package android.maa.traningapp.IntentForResult;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.maa.traningapp.R;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CheckStatusActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtBluetooth, txtWifi, txtLocation;
    Context mcontext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_status);

        Binding();
    }

    void Binding() {
        txtBluetooth = findViewById(R.id.txtBluetooth);
        txtWifi = findViewById(R.id.txtWif);
        txtLocation = findViewById(R.id.txtLocation);
        findViewById(R.id.btnBluetooth).setOnClickListener(this);
        findViewById(R.id.btnWifi).setOnClickListener(this);
        findViewById(R.id.btnLocation).setOnClickListener(this);
        checkBluetoothStatus();
        checkWifiStatus();
        checkLocationStatus();
    }

    void checkBluetoothStatus() {
        String result = "....";
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null)
            txtBluetooth.setText("doesn't support");
        else {
            if (mBluetoothAdapter.isEnabled())
                txtBluetooth.setText("ON");
            else {
                txtBluetooth.setText("OFF");

            }
        }

    }

    void checkWifiStatus() {
        /*String result = "....";
        ConnectivityManager mconnectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = mconnectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            txtWifi.setText("ON");
        } else {
            txtWifi.setText("OFF");
        }*/
        WifiManager wifiManager = (WifiManager) mcontext.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                txtWifi.setText("ON");
            } else {
                txtWifi.setText("OFF");
            }

        }
    }

    void checkLocationStatus() {

        LocationManager locationManager = (LocationManager) mcontext.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                txtLocation.setText("ON");
            } else {
                txtLocation.setText("OFF");
            }

        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnBluetooth) {
            Intent enableBtIntent = new Intent();
            enableBtIntent.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
            startActivityForResult(enableBtIntent, 100);

        } else if (view.getId() == R.id.btnWifi) {
            Intent mWIFI = new Intent();
            mWIFI.setAction(Settings.ACTION_WIFI_SETTINGS);
            startActivityForResult(mWIFI, 200);
        } else if (view.getId() == R.id.btnLocation) {
            Intent mLoaction = new Intent();
            mLoaction.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(mLoaction, 300);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            checkBluetoothStatus();
        } else if (requestCode == 200) {
            checkWifiStatus();
        } else if (requestCode == 300) {
            checkLocationStatus();
        }
    }
}
