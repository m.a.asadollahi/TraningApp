package android.maa.traningapp.SharePreference;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.maa.traningapp.R;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class SharePreferenceActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView userImage;
    EditText txtFirstName, txtLastName, txtNationalCode;
    Context mContext = this;
    Activity mActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_preference);
        Binding();
        LoadData();
    }

    void Binding() {
        txtFirstName = findViewById(R.id.txtFirstName);
        txtLastName = findViewById(R.id.txtLastName);
        txtNationalCode = findViewById(R.id.txtNationalCode);
        userImage = findViewById(R.id.userImg);

        findViewById(R.id.btnGetImage).setOnClickListener(this);
        findViewById(R.id.btnSave).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnGetImage) {
            EasyImage.openCamera(this, 10);
        }
        if (view.getId()==R.id.btnSave){
            SaveData("FirstName",txtFirstName.getText().toString());
            SaveData("LastName",txtLastName.getText().toString());
            SaveData("NationalCode",txtNationalCode.getText().toString());
            txtNationalCode.setText("");
            txtFirstName.setText("");
            txtLastName.setText("");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                Glide.with(mContext).load(imageFile).into(userImage);
            }
        });
    }

    void SaveData(String Key , String Value){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(Key,Value).apply();
    }
    String getData(String Key){
        String Result="";
        Result=PreferenceManager.getDefaultSharedPreferences(mContext).getString(Key,"");
        return Result;
    }
    void LoadData(){
        txtLastName.setText(getData("LastName"));
        txtFirstName.setText(getData("FirstName"));
        txtNationalCode.setText(getData("NationalCode"));
    }
}
