package android.maa.traningapp.Form;

import android.maa.traningapp.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterForm extends AppCompatActivity implements View.OnClickListener {

    EditText txtFirstName, txtLastName, txtNationalId;
    String firstName, lastName, nationalId;
    TextView txtRegisterResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_form);
        findViewById(R.id.btnRegister).setOnClickListener(this);
    }

    void BindingWidgets() {
        txtFirstName = findViewById(R.id.txtFirstName);
        txtLastName = findViewById(R.id.txtLastName);
        txtNationalId = findViewById(R.id.txtNationalID);
        txtRegisterResult = findViewById(R.id.txtRegisterResult);
    }

    void GetData() {
        firstName = txtFirstName.getText().toString();
        lastName = txtLastName.getText().toString();
        nationalId = txtNationalId.getText().toString();

    }

    void Register() {
        BindingWidgets();
        GetData();
        String Result = String.format(getString(R.string.RegisterResult)+"\n"+
                getString(R.string.RegisterResult_Name) +"%s \n "+
                getString(R.string.RegisterResult_LastName) +"%s \n"+
                getString(R.string.RegisterResult_NationalID) +"%s", firstName, lastName, nationalId);
        txtRegisterResult.setText(Result);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnRegister) {
            Register();
        }
    }
}
