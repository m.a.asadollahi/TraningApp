package android.maa.traningapp.WebService;

import android.content.Context;
import android.maa.traningapp.PublicTools.PublicMethod;
import android.maa.traningapp.R;
import android.maa.traningapp.WeatherClass.Forecast;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class forcastAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forecasts;

    public forcastAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int i) {
        return forecasts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View row = LayoutInflater.from(mContext).inflate(R.layout.forcast_list_item, viewGroup, false);

        TextView day = row.findViewById(R.id.txtDay);
        TextView high = row.findViewById(R.id.txtForcstHighTemp);
        TextView low = row.findViewById(R.id.txtForcastLowTemp);
        TextView StatusWeather = row.findViewById(R.id.txtStatusWeather);
        TextView WeatherIcon = row.findViewById(R.id.txtweatherIcon);
        WeatherIcon.setTypeface(PublicMethod.weatherTypeFace(mContext));
        TextView Date = row.findViewById(R.id.txtDate);

        day.setText(forecasts.get(i).getDay());
        high.setText(PublicMethod.convertFtoC(forecasts.get(i).getHigh()));
        low.setText(PublicMethod.convertFtoC(forecasts.get(i).getLow()));
        StatusWeather.setText(forecasts.get(i).getText());
        Date.setText(forecasts.get(i).getDate());
        int weatherCode = Integer.parseInt(forecasts.get(i).getCode());
        WeatherIcon.setText(PublicMethod.getWeatherIcon(mContext, weatherCode));


        return row;
    }
}
