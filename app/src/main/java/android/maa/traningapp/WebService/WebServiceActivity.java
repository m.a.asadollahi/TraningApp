package android.maa.traningapp.WebService;


import android.app.ProgressDialog;
import android.content.Context;
import android.maa.traningapp.PublicTools.PublicMethod;
import android.maa.traningapp.R;
import android.maa.traningapp.WeatherClass.Forecast;
import android.maa.traningapp.WeatherClass.YahooWeather;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class WebServiceActivity extends AppCompatActivity implements View.OnClickListener {
    TextView  txtTemp;
    EditText txtCity;
    Button btnGet, btnWeather;
    ListView weatherList;
    String IP_API_URL = "http://ip-api.com/json";
    Context mContext = this;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);
        bind();

    }

    void bind() {
        txtCity = findViewById(R.id.txtCity);
        btnGet = findViewById(R.id.btnGet);
        btnGet.setOnClickListener(this);
        btnWeather = findViewById(R.id.btnGetWeather);
        btnWeather.setOnClickListener(this);
        txtTemp = findViewById(R.id.txtTemp);
        weatherList = findViewById(R.id.weatherList);
        pDialog = new ProgressDialog(mContext);
        pDialog.setTitle("Loading");
        pDialog.setMessage("Please Waite .... ");
    }

    void getCityName() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(IP_API_URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethod.toast(mContext, "Error");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndSetCity(responseString);
            }
        });
    }

    void parseAndSetCity(String ServiceResponse) {
        try {
            JSONObject jobject = new JSONObject(ServiceResponse);
            String cityName = jobject.getString("city");
            txtCity.setText(cityName);
        } catch (Exception e) {
            PublicMethod.toast(mContext, e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnGet)
            getCityName();
        if (view.getId() == R.id.btnGetWeather)
            getWeather(txtCity.getText().toString());
    }

    void getWeather(String cityName) {
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                cityName +
                "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        pDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethod.toast(mContext, "Error to connect ....");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseWeatherString(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pDialog.dismiss();
            }
        });
    }

    void parseWeatherString(String webServiceString) {
        Gson gson = new Gson();
        YahooWeather yahooWeather = gson.fromJson(webServiceString, YahooWeather.class);
        txtTemp.setText(yahooWeather.getQuery().getResults().getChannel().getItem().getCondition().getTemp());

        List<Forecast> forecasts = yahooWeather.getQuery().getResults().getChannel().getItem().getForecast();
        forcastAdapter adapter = new forcastAdapter(mContext,forecasts);
        weatherList.setAdapter(adapter);

    }
}
