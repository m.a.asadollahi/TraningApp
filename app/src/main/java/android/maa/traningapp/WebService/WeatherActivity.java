package android.maa.traningapp.WebService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.maa.traningapp.PublicTools.PublicMethod;
import android.maa.traningapp.R;
import android.maa.traningapp.WeatherClass.Forecast;
import android.maa.traningapp.WeatherClass.YahooWeather;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtCityName,txtWeather,txtTemp,txtToday,txtHighTemp,txtLowTemp;
    EditText txtCity;
    ListView forcastListView;
    String IP_API_URL = "http://ip-api.com/json";
    Context mContext = this;
    ProgressDialog pDialog;
    Button btnShowWeather;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        bind();

        //getCityName();
        getWeather("tehran");

    }

    void bind() {
        txtCityName = findViewById(R.id.txtCityName);
        txtWeather = findViewById(R.id.txtweather);
        txtTemp = findViewById(R.id.txtTemp);
//        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/weather.ttf");
        txtWeather.setTypeface(PublicMethod.weatherTypeFace(mContext));
        txtToday=findViewById(R.id.txtToday);
        txtLowTemp=findViewById(R.id.txtLowTemp);
        txtHighTemp=findViewById(R.id.txtHighTemp);
        forcastListView=findViewById(R.id.forcastList);
        pDialog = new ProgressDialog(mContext);
        pDialog.setTitle("Please Wait");
        pDialog.setMessage("Connection to yahoo server");
        btnShowWeather=findViewById(R.id.btnShowWeather);
        btnShowWeather.setOnClickListener(this);
        txtCity=findViewById(R.id.txtCity);
    }

    void getCityName() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(IP_API_URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                txtTemp.setText("0");
                txtToday.setText("---");
                txtWeather.setText("");
                txtHighTemp.setText("0");
                txtLowTemp.setText("0");
                PublicMethod.toast(mContext, "Connection error ....");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                setCityName(responseString);
            }
        });
    }

    void setCityName(String ServceResponse) {
        try {
            JSONObject jsonObject = new JSONObject(ServceResponse);
            String cityName = jsonObject.getString("city").toLowerCase();
            txtCityName.setText(cityName);
            txtCity.setText(cityName);
            getWeather(cityName);

        } catch (Exception error) {
            PublicMethod.toast(mContext, error.getMessage());
        }
    }

    void getWeather(String cityName) {
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                cityName +
                "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        pDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                txtTemp.setText("0");
                txtToday.setText("---");
                txtWeather.setText("");
                txtHighTemp.setText("0");
                txtLowTemp.setText("0");
                PublicMethod.toast(mContext, "Connection error ....");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseWeatherResponse(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                pDialog.dismiss();
            }
        });
    }

    void parseWeatherResponse(String ServiceRespons) {
        Gson gson = new Gson();
        YahooWeather yahooWeather = gson.fromJson(ServiceRespons,YahooWeather.class);
        String cTemp = PublicMethod.convertFtoC(yahooWeather.getQuery().getResults().getChannel().getItem().getCondition().getTemp());
        txtTemp.setText(cTemp);
        int weatherCode = Integer.parseInt(yahooWeather.getQuery().getResults().getChannel().getItem().getCondition().getCode());
        String today = yahooWeather.getQuery().getResults().getChannel().getItem().getForecast().get(0).getDay()+","+
                yahooWeather.getQuery().getResults().getChannel().getItem().getForecast().get(0).getDate();
        txtToday.setText(today);
        txtWeather.setText(PublicMethod.getWeatherIcon(mContext, weatherCode));
        cTemp = PublicMethod.convertFtoC( yahooWeather.getQuery().getResults().getChannel().getItem().getForecast().get(0).getHigh());
        txtHighTemp.setText(cTemp);
        cTemp = PublicMethod.convertFtoC( yahooWeather.getQuery().getResults().getChannel().getItem().getForecast().get(0).getLow());
        txtLowTemp.setText(cTemp);

        List<Forecast> forcastList = yahooWeather.getQuery().getResults().getChannel().getItem().getForecast();
        forcastAdapter adapter = new forcastAdapter(mContext,forcastList);
        forcastListView.setAdapter(adapter);

    }

    @Override
    public void onClick(View view) {
        String city= txtCity.getText().toString().toLowerCase();
        txtCityName.setText(city);
        getWeather(city);

    }
}
