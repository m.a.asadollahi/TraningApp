package android.maa.traningapp.Intent;

import android.maa.traningapp.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DestinationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);

        String firstName = getIntent().getStringExtra("FirstName");
        String lastName = getIntent().getStringExtra("LastName");
        ((TextView)findViewById(R.id.txtResult)).setText("Your Enterd :" +"\n" +firstName+" "+lastName);
    }

}
