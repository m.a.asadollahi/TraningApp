package android.maa.traningapp.Intent;

import android.content.Intent;
import android.maa.traningapp.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class OriginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText firstName, lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origin);
        Bind();
    }

    void Bind() {
        firstName = findViewById(R.id.txtFirstName);
        lastName = findViewById(R.id.txtLastName);
        findViewById(R.id.btnSubmit).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(OriginActivity.this, DestinationActivity.class);
        intent.putExtra("FirstName", firstName.getText().toString());
        intent.putExtra("LastName", lastName.getText().toString());
        startActivity(intent);
    }
}
