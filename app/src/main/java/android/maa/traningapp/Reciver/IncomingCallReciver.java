package android.maa.traningapp.Reciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.maa.traningapp.ListView.IncomingDetails;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.TabHost;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class IncomingCallReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Date currentTime = Calendar.getInstance().getTime();
        String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

        IncomingDetails call = new IncomingDetails();
        call.setPhoneNumber(incomingNumber);
        call.setDate(currentTime.toString());

        List<IncomingDetails> callList;
        callList = Hawk.get("callList");
        if (callList == null) {
            callList = new ArrayList<>();
            callList.add(call);
        } else
            callList.add(call);
        Hawk.put("callList", callList);


    }
}
