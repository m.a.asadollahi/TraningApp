package android.maa.traningapp.Reciver;

import android.maa.traningapp.ListView.IncomingDetails;
import android.maa.traningapp.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

public class ReciverActivity extends AppCompatActivity {
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reciver);
        txt = findViewById(R.id.txt);

        List<IncomingDetails> callList = new ArrayList<>();
        callList=Hawk.get("callList");

        for (IncomingDetails call: callList
             ) {
            txt.setText(txt.getText()+"\n"+ call.getPhoneNumber()+" - "+ call.getDate());
            
        }
        

    }
}
