package android.maa.traningapp.ListView;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.maa.traningapp.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AppListAdapter extends BaseAdapter {
    Context mContext;
    List<AppItem> listBtn ;

    public AppListAdapter(Context mContext, List<AppItem> listBtn) {
        this.mContext = mContext;
        this.listBtn = listBtn;
    }

    @Override
    public int getCount() {
        return listBtn.size();
    }

    @Override
    public Object getItem(int index) {
        return listBtn.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public View getView(int index, View view, ViewGroup viewGroup) {
        View row = LayoutInflater.from(mContext).inflate(R.layout.app_item,viewGroup,false);
        ImageView img = row.findViewById(R.id.img);
        TextView txt = row.findViewById(R.id.appItemName);

        txt.setText(listBtn.get(index).getName());
       img.setImageResource(listBtn.get(index).getImageIndex());
        return row;
    }
}
