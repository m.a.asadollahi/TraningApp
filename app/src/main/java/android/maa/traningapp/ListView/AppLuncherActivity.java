package android.maa.traningapp.ListView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.maa.traningapp.Form.RegisterForm;
import android.maa.traningapp.Intent.OriginActivity;
import android.maa.traningapp.IntentForResult.CheckStatusActivity;
import android.maa.traningapp.LayoutPracticeActivity;
import android.maa.traningapp.R;
import android.maa.traningapp.Reciver.ReciverActivity;
import android.maa.traningapp.SharePreference.SharePreferenceActivity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AppLuncherActivity extends AppCompatActivity {
    ListView myAppList;
    Context mcontext = this;
    Activity mActivity=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_luncher);
        checkPermission();
        Bind();
        AppListAdapter appadapt = new AppListAdapter(this, BindAppItems());
        myAppList.setAdapter(appadapt);
    }

    List<AppItem> BindAppItems() {

        List<AppItem> items = new ArrayList<>();

        AppItem appItem = new AppItem();
        appItem.setName("Layout");
        appItem.setImageIndex(R.drawable.app);
        items.add(appItem);

        appItem = new AppItem();
        appItem.setName("Register Form");
        appItem.setImageIndex(R.drawable.app);
        items.add(appItem);

        appItem = new AppItem();
        appItem.setName("Intent");
        appItem.setImageIndex(R.drawable.app);
        items.add(appItem);

        appItem = new AppItem();
        appItem.setName("Intent For Result");
        appItem.setImageIndex(R.drawable.app);
        items.add(appItem);

        appItem = new AppItem();
        appItem.setName("Share Preference");
        appItem.setImageIndex(R.drawable.app);
        items.add(appItem);

        appItem = new AppItem();
        appItem.setName("Reciver");
        appItem.setImageIndex(R.drawable.app);
        items.add(appItem);

        return items;
    }

    void Bind() {
        myAppList = findViewById(R.id.appLuncherList);
        myAppList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

               if (i==0){
                   Intent in = new Intent(AppLuncherActivity.this, LayoutPracticeActivity.class);
                   startActivity(in);
               }
                if (i==1){
                    Intent in = new Intent(AppLuncherActivity.this, RegisterForm.class);
                    startActivity(in);
                }
                if (i==2){
                    Intent in = new Intent(AppLuncherActivity.this, OriginActivity.class);
                    startActivity(in);
                }
                if (i==3){
                    Intent in = new Intent(AppLuncherActivity.this, CheckStatusActivity.class);
                    startActivity(in);
                }
                if (i==4){
                    Intent in = new Intent(AppLuncherActivity.this, SharePreferenceActivity.class);
                    startActivity(in);
                }
                if (i==5){
                    Intent in = new Intent(AppLuncherActivity.this, ReciverActivity.class);
                    startActivity(in);
                }

            }
        });
    }
    void checkPermission() {
        if (ContextCompat.checkSelfPermission(mcontext,
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    1000);
        }
    }
}
